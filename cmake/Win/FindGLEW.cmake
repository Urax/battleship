find_library(GLEW_LIB 
    NAMES glew32
    PATHS ${LIB_SEARCH_PATH}
)

set(LIBS ${LIBS} ${GLEW_LIB})
