#version 330 core

in vec2 TextCord;
in vec3 Color;

out vec4 outColor;

uniform sampler2D Texture;

void main() {
    outColor = texture(Texture, TextCord) * vec4(Color, 1.0);
}
