#version 330 core

in vec2 position;
in vec3 color;
in vec2 textCord;

uniform mat4 projection;

out vec3 Color;
out vec2 TextCord;

void main(){
    Color = color;
    gl_Position = projection * vec4(position, 0.0, 1.0) ;
    TextCord = textCord;
}

