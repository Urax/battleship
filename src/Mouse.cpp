#include "Mouse.h"

Mouse::Mouse()
{
}

Mouse::Mouse(
        shared_board enemyBoard, 
        shared_board playerBoard, 
        shared_ptr<BoardsInfo> bsInfo, 
        shared_ptr<vector<Button>> buttons,
        shared_ptr<EVENTS> events,
        shared_ptr<GAME_STATE> gameState
    )
{
    this->bsInfo = bsInfo;
    this->enemyBoard = enemyBoard;
    this->playerBoard = playerBoard;
    this->buttons = buttons;
    this->events = events;
    this->gameState = gameState;

    row = std::make_shared<int>(0);
    column = std::make_shared<int>(0);
}


void Mouse::ChangeBoard(bool leftB){
    float board_y = *gameState ? bsInfo->plY : bsInfo->enY;
    *row = (y - board_y) / bsInfo->unit;
    *column = (x - bsInfo->x) / bsInfo->unit;
    switch(*gameState)
    {
        case SETUP:
            if(leftB)
            {
                switch (playerBoard->data()[*row][*column])
                {
                    case EMPTY:
                        playerBoard->data()[*row][*column] = ALIVE;
                        break;
                    default:
                        break;
                } 
            }
            else
            {
                switch (playerBoard->data()[*row][*column])
                {
                    case ALIVE:
                        playerBoard->data()[*row][*column] = EMPTY;
                        break;
                    default:
                        break;
                } 
            }
            break;
    
        case PLAY:
        {
            switch (enemyBoard->data()[*row][*column])
            {
                case EMPTY:
                    enemyBoard->data()[*row][*column] = MISS;
                    *events = CHECK;
                    break;
                case ALIVE:
                    enemyBoard->data()[*row][*column] = HIT;
                    *events = CHECK;
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

void Mouse::ShareRowCol(shared_ptr<int> row, shared_ptr<int> col)
{
    this->row = row;
    this->column = col;
}

bool Mouse::CheckInBoard()
{
    float board_y = *gameState ? bsInfo->plY : bsInfo->enY;
    return CheckInRange(bsInfo->x, bsInfo->x + bsInfo->width, board_y, board_y + bsInfo->height);
}

bool Mouse::CheckInRange(float rangeX0, float rangeX1, float rangeY0, float rangeY1)
{
    if(
        (x > rangeX0 && x < rangeX1) &&
        (y > rangeY0 && y < rangeY1)
      )
    {   
        return true;
    }
    else
    {
        return false;
    }
    
} 

BUTTON_NAME Mouse::CheckButtons()
{
    float bx0, by0, bx1, by1;
    for(int i = 0; i < buttons->size(); ++i)
    {
        bx0 = buttons->data()[i].x; 
        bx1 = bx0 + buttons->data()[i].lenX;
        by0 = buttons->data()[i].y; 
        by1 = by0 + buttons->data()[i].lenY;
        if(CheckInRange(bx0, bx1, by0, by1))
        {
            return buttons->data()[i].name;
        }
    }
    return NONE;
}

BUTTON_NAME Mouse::GetPos()
{
    SDL_PumpEvents();
    Uint32 button = SDL_GetMouseState(&x, &y);
    if ((button & SDL_BUTTON_LMASK) != 0)
    {
        btnName = CheckButtons();
        if(CheckInBoard())
        {
            ChangeBoard(true);
        }
        return btnName;
    }
    else if ((button & SDL_BUTTON_MMASK) != 0)
    {
        std::cout << "Scroll button pressed!" << std::endl;
    }
    else if ((button & SDL_BUTTON_RMASK) != 0)
    {
        if(CheckInBoard())
        {
            ChangeBoard(false);
        }
        std::cout << "Right button pressed!" << std::endl;
    }
    return NONE;
}
