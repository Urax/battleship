#include "Graphics.h"


bool Graphics::Init(int s_width, int s_height, shared_board enemyBoard, shared_board playerBoard, shared_ptr<EVENTS> events, shared_ptr<GAME_STATE> gameState) {

    if(shaders.ShaderInit())
    {
        this->s_width = s_width;
        this->s_height = s_height;
        this->enemyBoard = enemyBoard;
        this->playerBoard = playerBoard;
        this->events = events;
        this->gameState = gameState;

        BoardsInfo tmp_bi = {0., 0., 0., 0., 0., 0.};
        bsInfo = std::make_shared<BoardsInfo>(tmp_bi);
        buttons = std::make_shared<vector<Button>>();
        CalculateBoardsInfo();
        
        mouse = Mouse(enemyBoard, playerBoard, bsInfo, buttons, events, gameState);
        Projection();
        RegisterButton(ACCEPT, "Ustaw", 0.7*s_width, 0.5*s_height, 0.2*s_width,0.1*s_height);
        return true;
    }
    else return false;
}

void Graphics::Projection()
{
    shaders.Projection(s_width, s_height);
}

void Graphics::CalculateBoardsInfo()
{
    bsInfo->plY = 0.6*s_height;
    bsInfo->enY = 0.1*s_height;
    bsInfo->height = 0.3*s_height;
    int rows = playerBoard->size();
    int columns = playerBoard->data()[0].size();
    bsInfo->unit = bsInfo->height / rows;
    if(bsInfo->unit * columns > 0.8*s_width)
    {
        bsInfo->x = 0.1*s_width;
        bsInfo->width = 0.8*s_width;
        bsInfo->unit = bsInfo->width / columns;
        bsInfo->height = bsInfo->unit * rows;
    }
    else
    {
        bsInfo->width = bsInfo->unit*columns;
        bsInfo->x = (s_width - bsInfo->width)/2;
    }
}

void Graphics::DrawBoards()
{
    shaders.SetBgrnd(DARK_BLUE);
    DrawBoard(PLAYER);
    DrawBoard(ENEMY);
}

void Graphics::Draw()
{
    DrawBoards();
    DrawButtons();
}

void Graphics::DrawBoard(TURN turn)
{
    vector<SQ_STATE, std::allocator<SQ_STATE>> * board_data;
    float a = bsInfo->unit;
    float curX; 
    COL color;
    float curY = bsInfo->plY;
       
    if(turn)
        board_data = playerBoard->data();
    else
    {
        board_data = enemyBoard->data();
        curY = bsInfo->enY;
    }

    int n = board_data->size();
    for(int i = 0; i < n; ++i)
    {
        curX = bsInfo->x;
        for(int j = 0; j < board_data[i].size(); ++j)
        {
            if(turn)
            {
                switch(board_data[i][j])
                {
                    case EMPTY:
                        color = AQUA;
                        break;
                    case ALIVE:
                        color = GREY;
                        break;
                    case HIT:
                        color = RED;
                        break;
                    case MISS:
                        color = BLACK;
                        break;
                }
            }
            else
            {
                switch(board_data[i][j])
                {
                    case HIT:
                        color = RED;
                        break;
                    case MISS:
                        color = BLACK;
                        break;
                    default:
                        color = AQUA;
                        break;
                }
            }
            shaders.DrawSquare(curX, curY, a, color);
            shaders.DrawStrLine(curX, curY, a, YELLOW);        
            shaders.DrawStrLine(curX, curY, a, YELLOW, HOR);        
            curX += a;
        }
        shaders.DrawStrLine(curX, curY, a, YELLOW);        
        curY += a;
    }
    curX = bsInfo->x;
    for(int j = 0; j < board_data[n-1].size(); ++j)
    {
        shaders.DrawStrLine(curX, curY, a, YELLOW, HOR);        
        curX += a;
    }
}

void Graphics::ShareRowColToMouse(shared_ptr<int> row, shared_ptr<int> column)
{
    mouse.ShareRowCol(row, column);
}

void Graphics::RegisterButton(BUTTON_NAME name, string text, float x, float y, float lenX, float lenY)
{
    Button tmp_button = {
        .name = name,
        .text = text,
        .x = x,
        .y = y,
        .lenX = lenX,
        .lenY = lenY
    };

    buttons->push_back(tmp_button);
}

void Graphics::DrawButtons()
{
    float x;
    float y;
    float lenX;
    float lenY;
    string text;
    for(int i = 0; i < buttons->size(); ++i)
    {
        x = buttons->data()[i].x;
        y = buttons->data()[i].y;
        lenX = buttons->data()[i].lenX;
        lenY = buttons->data()[i].lenY;
        text = buttons->data()[i].text;
        shaders.DrawRect(x, y, lenX, lenY, GREEN);
        shaders.LoadText(text, x+0.1*lenX, y+0.1*lenY, lenX-0.2*lenX, BLACK);
    }
}

void Graphics::GetPos()
{
    BUTTON_NAME res = mouse.GetPos();
    switch(res)
    {
        case ACCEPT:
            cout << "Wcisnieto accept" << endl;
           *events = SET; 
           break;
        default:
           break;
    }
}
