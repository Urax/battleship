#include <iostream>
#include <vector>

#include "Mouse.h"

using std::vector;

using state_board = vector<vector<SQ_STATE>>;

class Bot
{
    
    state_board board;

    int rows, columns;
    shared_ptr<vector<int *>> amShips;  
    vector<int *> previousRnC;
    int * ChoseRnC;
    bool CheckNearby();

    // check everything surrounding except for the ones u used, u can make temporary pointer to hold row and column of the chosen blocks

public:

    Bot();
    Bot(int rows, int columns, shared_ptr<vector<int *>> amShips);
    state_board Generate();
};
