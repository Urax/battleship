#include "Logic.h"

Logic::Logic()
{
     
}

Logic::Logic(int rows, int columns)  
{
    this->rows = rows;
    this->columns = columns;
    InitBoards();

    
    int def_set[4][2] = {{4, 1}, {3, 2}, {2, 3}, {1, 4}};

    vector<int *> tmp (std::begin(def_set), std::end(def_set));
    amShips = std::make_shared<vector<int *>>(tmp);
    //int test[2] = {0, 5};
    //amShips->push_back(test);

    curRow = std::make_shared<int>(0);
    curCol = std::make_shared<int>(0);


    events = std::make_shared<EVENTS> (NOTHING);

    /*
    bot = Bot(rows, columns, amShips);
    GenerateBotBoard();
    */
}

void Logic::InitBoards()
{
    vector<vector<SQ_STATE>> tmp_board;
    vector<SQ_STATE> supp(columns, EMPTY);
    for(int i = 0; i < rows; ++i)
        tmp_board.push_back(supp);

    helpBoard = tmp_board;
    // init boards pointers with values from vector "tmp_board"
    playerBoard = std::make_shared<vector<vector<SQ_STATE>>>(tmp_board);
    enemyBoard = std::make_shared<vector<vector<SQ_STATE>>>(tmp_board);
}

/*
void Logic::GenerateBotBoard()
{
    helpBoard = bot.Generate();
    enemyBoard = std::make_shared<vector<vector<SQ_STATE>>>(helpBoard);
}
*/

shared_ptr<int> Logic::GetRowPtr()
{
    return curRow;
}

shared_ptr<int> Logic::GetColPtr()
{
    return curCol;
}

shared_board Logic::GetPlayerBoard()
{
    return playerBoard;
} 

shared_board Logic::GetEnemyBoard()
{
    return enemyBoard;
}

shared_ptr<EVENTS> Logic::GetEvents()
{
    return events;
}

bool Logic::CheckBoard(bool player)
{
    vector<SQ_STATE, std::allocator<SQ_STATE>> * board_data;
    int res = 0;
    
    if(player)
    {
        board_data = playerBoard->data();
    }
    else
    {
        board_data = enemyBoard->data();
    }


    for(int i = 0; i < *curRow; ++i)
    {
        for(int j = 0; j < *curCol; ++j)
        {
            if(board_data[i][j] == ALIVE)
                res++;
        }
    }
    if(!res)
        return true;
    return false;
}

void Logic::CheckWin()
{
    /*
   bool pwin = CheckBoard(true);
   bool ewin = CheckBoard(false);
   if(!pwin){
       *events = PLAYER_WIN;
   }
   else if(!ewin)
   {
       *events = ENEMY_WIN;
   }
   */
}

void Logic::ResetBoards()
{
    playerBoard->clear();
    enemyBoard->clear();
    InitBoards();
}

GAME_STATE Logic::CheckEvent()
{
    switch (*events)
    {
        case CHECK:
            CheckWin();
            return PLAY;
            break;
        case SET:
            return PLAY;
            break;
        case RESET:
            ResetBoards();
            // reset to previous state
            *events = NOTHING;
            return SETUP;
            break;
        default:
            return SETUP;
            break;
    }
}
