#pragma once

#include "Mouse.h"
#include "ShaderLoader.h"

#include <iostream>
#include <memory>
#include <vector>

enum TURN
{
    ENEMY = 0,
    PLAYER = 1,
};

class Graphics {

    int s_width, s_height;
    shared_ptr<BoardsInfo> bsInfo;
    shared_board playerBoard;
    shared_board enemyBoard;
    shared_ptr<EVENTS> events;
    shared_ptr<GAME_STATE> gameState;

    shared_ptr<vector<Button>> buttons;

    Mouse mouse;
    Shader shaders;

    void Projection();
    void RegisterButton(BUTTON_NAME name, string text, float x, float y, float lenX, float lenY);
    void CalculateBoardsInfo();
    void DrawBoard(TURN turn);
    void DrawBoards();
    void DrawButtons();

    public:
        bool Init(
                int s_width, int s_height, 
                shared_board enemyBoard, 
                shared_board playerBoard, 
                shared_ptr<EVENTS> events, 
                shared_ptr<GAME_STATE> gameState
            );
        bool Init(int s_width, int s_height, shared_board enemyBoard, shared_board playerBoard, shared_ptr<EVENTS> events); 
        void ShareRowColToMouse(shared_ptr<int> row, shared_ptr<int> column);
        void Draw();
        void GetPos();

};
