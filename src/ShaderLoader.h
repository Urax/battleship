#pragma once

#include <GL/glew.h>
#include <GL/gl.h>

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>

#include "FontHandler.h"

#include <glm/ext/matrix_float4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

#define TEX_SHADER 0
#define OBJ_SHADER 1

#define MAGIC_NUM 0.866 // sqrt(3)/2

using std::string;
using std::ifstream;
using std::cout;

enum COL
{
    BLACK = 0,
    RED,
    GREEN,
    BLUE,
    DARK_BLUE,
    AQUA,
    YELLOW,
    PURPLE,
    GREY,
    WHITE
};   

enum ORIENT
{
    HOR = 0, // horizontal
    VER,     // vertical
};
    
class Shader
{


    struct Color
    {
        float r;
        float g;
        float b;
    };
    GLuint objShader;
    GLuint LoadShaders(const char* vertex_file, const char* fragment_file);
    struct Color * colors;

public:

    Shader();
    ~Shader();
    void SetBgrnd(COL col);
    GLuint texShader;
    bool ShaderInit();
    // ONLY USE ON IMAGES, NOT ON FONTS
    bool PrepareImg(string textureName);
    void Projection(int width, int height);
    void DrawSquare(float x, float y, float len, COL col);
    void DrawRect(float x, float y, float lenX, float lenY, COL col);
    void DrawIsTriangle(float x, float y, float lenX, float lenY, COL col, ORIENT orient = VER); // Isolesceles -  rownoramienny
    void DrawEqTriangle(float x, float y, float len, COL col, ORIENT orient = VER);
    void DrawStrLine(float x, float y, float len, COL col, ORIENT orient = VER);
    void SetObjShader(float * vertices, size_t vSize, GLenum SHAPE = GL_QUADS);
    void SetTexShader(float * vertices, size_t vSize);
    void LoadText(string & text, float x, float y, float length, COL col);

};
