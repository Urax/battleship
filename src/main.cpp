#include "Game.h"


int main(int argc, char **argv)
{
    {
        Game game;
        if(!game.Init())
        {
            std::cout << "ERROR" << std::endl;
            return 1;
        }
        game.Loop();
    }
    return 0;
}
