#include "FontHandler.h"

FontHandler::FontHandler()
{
    bl = rd = gr = 1.0f;
}

bool FontHandler::Load(string & fname)
{
    char *dat,*img;
    ifstream in(fname, std::ios::binary);
    unsigned long fileSize;
    char bpp;
    int imgX,imgY;


    if(in.fail())
        return false;

    // Get Filesize
    in.seekg(0,std::ios::end);
    fileSize=in.tellg();
    in.seekg(0,std::ios::beg);

    // allocate space for file data
    dat=new char[fileSize];

    // Read filedata
    if(!dat)
    return false;

    in.read(dat,fileSize);

    if(in.fail())
    {
        delete [] dat;
        in.close();
        return false;
    }

    in.close();

    // Check ID is 'BFF2'
    if((unsigned char)dat[0]!=0xBF || (unsigned char)dat[1]!=0xF2)
    {
        delete [] dat;
        return false;
    }
    // Grab the rest of the header
    memcpy(&imgX,&dat[2],sizeof(int));
    memcpy(&imgY,&dat[6],sizeof(int));
    memcpy(&cellX,&dat[10],sizeof(int));
    memcpy(&cellY,&dat[14],sizeof(int));

    bpp=dat[18];
    base=dat[19];

    // Check filesize
    if(fileSize!=((MAP_DATA_OFFSET)+((imgX*imgY)*(bpp/8))))
        return false;

    // Calculate font params
    rowPitch = imgX / cellX;
    colFactor = (float)cellX / (float)imgX;
    rowFactor = (float) cellY/(float)imgY;
    yOffset = cellY;

    // Determine blending options based on BPP
    switch(bpp)
    {
     case 8: // Greyscale
        renderStyle = BFG_RS_ALPHA;
        break;

     case 24: // RGB
        renderStyle = BFG_RS_RGB;
        break;

     case 32: // RGBA
        renderStyle = BFG_RS_RGBA;
        break;

     default: // Unsupported BPP
        delete [] dat;
        return false;
        break;
    }

    // Allocate space for image
    img = new char[(imgX*imgY) * (bpp / 8)];

    if(!img)
    {
       delete [] dat;
       return false;
    }

    // Grab char widths
    memcpy(width,&dat[WIDTH_DATA_OFFSET],256);

    // Grab image data
    memcpy(img,&dat[MAP_DATA_OFFSET],(imgX*imgY)*(bpp/8));

    // Create Texture
    glGenTextures(1,&texID);
    glBindTexture(GL_TEXTURE_2D,texID);
    // Fonts should be rendered at native resolution so no need for texture filtering
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST); 
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    // Stop chararcters from bleeding over edges
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

    // Tex creation params are dependent on BPP
    switch(renderStyle)
    {
     case BFG_RS_ALPHA:
      glTexImage2D(GL_TEXTURE_2D,0,GL_LUMINANCE,imgX,imgY,0,GL_LUMINANCE,GL_UNSIGNED_BYTE,img);
      break;

     case BFG_RS_RGB:
      glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,imgX,imgY,0,GL_RGB,GL_UNSIGNED_BYTE,img);
      break;

     case BFG_RS_RGBA:
      glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,imgX,imgY,0,GL_RGBA,GL_UNSIGNED_BYTE,img);
      break;
    }
    // Clean up
    delete [] img;
    delete [] dat;
  
    return true;
}


// The texture ID is a private member, so this function performs the texture bind

void FontHandler::Bind()
{
    glBindTexture(GL_TEXTURE_2D,texID);
}

// Set the color and blending options based on the Renderstyle member
void FontHandler::SetBlend()
{
    switch(renderStyle)
     {
      case BFG_RS_ALPHA: // 8Bit
       glBlendFunc(GL_SRC_ALPHA,GL_SRC_ALPHA);
       glEnable(GL_BLEND);
       break;

      case BFG_RS_RGB:   // 24Bit
       glDisable(GL_BLEND);
       break;

      case BFG_RS_RGBA:  // 32Bit
       glBlendFunc(GL_ONE,GL_ONE_MINUS_SRC_ALPHA);
       glEnable(GL_BLEND);
       break;
     }
}

// Shortcut, Enables Texturing and performs Bind and SetBlend
void FontHandler::Select()
{
    glEnable(GL_TEXTURE_2D);
    Bind();
    SetBlend();
}

// Set the font color NOTE this only sets the polygon color, the texture colors are fixed
void FontHandler::SetColor(float red, float green, float blue)
{
    rd=red;
    gr=green;
    bl=blue;
}



void FontHandler::Set(float * vertices, size_t vSize, GLuint shaderID, short SHADER_T, GLenum SHAPE)
{

    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, vSize, vertices, GL_STATIC_DRAW);

    GLint posLoc = glGetAttribLocation(shaderID, "position");
    GLint colLoc = glGetAttribLocation(shaderID, "color");
    GLint textCord;

    int stride = 5;

    // 0 - texture
    // 1 - object
    if(!SHADER_T)
    {
        textCord = glGetAttribLocation(shaderID, "textCord");
        glEnableVertexAttribArray(textCord);
        stride+=2;
    }

    glEnableVertexAttribArray(posLoc);
    glEnableVertexAttribArray(colLoc);

    glVertexAttribPointer(
       posLoc,
       2,                  // size
       GL_FLOAT,           // type
       GL_FALSE,           // normalized?
       stride*sizeof(float),    // stride
       (void*)0            // array buffer offset
    );

    glVertexAttribPointer(
        colLoc,
        3,
        GL_FLOAT,
        GL_FALSE,
        stride*sizeof(float),
        (void*)(2*sizeof(float))
    );

    if(!SHADER_T)
    {
        glVertexAttribPointer(
            textCord,
            2,
            GL_FLOAT,
            GL_FALSE,
            stride*sizeof(float),
            (void*)(5*sizeof(float))
        );
    }

    switch(SHAPE) 
    {
        case GL_QUADS: 
            glDrawArrays(GL_QUADS, 0, 4); 
            break;
        case GL_LINES:
            glDrawArrays(GL_LINES, 0, 2); 
            break;
        case GL_TRIANGLES:
            glDrawArrays(GL_TRIANGLES, 0, 3);
            break;
    }

    glDisableVertexAttribArray(posLoc);
    glDisableVertexAttribArray(colLoc);
    glDisableVertexAttribArray(textCord);
}


void FontHandler::CalcUnit(const char *text, int sLen, float length)
{
    unit = 0;
    for(int loop = 0; loop != sLen; ++loop)
    {
        unit += width[text[loop]];
    }
    unit = length / unit;
}

void FontHandler::Print(const char* text, float x, float y, float length, GLuint texShader)
{
    int sLen,loop;
    int row,col;
    float u,v,u1,v1;
    float curX = x;

    sLen=(int)strnlen(text,BFG_MAXSTRING);

    CalcUnit(text, sLen, length);


    for(loop=0;loop!=sLen;++loop)
    {
        row = (text[loop] - base) / rowPitch;
        col = (text[loop] - base) - row * rowPitch;

        u = col * colFactor;
        v = row * rowFactor;
        u1 = u + colFactor;
        v1 = v + rowFactor;

        float vertices[] = {
            // positions                       // colors     // texture coords
             curX + cellX*unit,  y,            rd, gr, bl,   u1, v,   // top right
             curX + cellX*unit, y+cellY*unit,  rd, gr, bl,   u1, v1,   // bottom right
             curX, y+cellY*unit,               rd, gr, bl,   u, v1,   // bottom left
             curX,  y,                         rd, gr, bl,   u, v    // top left 
        };

        curX += width[text[loop]]*unit;
        Set(vertices, sizeof(vertices), texShader, 0);
    }
}

