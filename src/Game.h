#pragma once

#define SDL_MAIN_HANDLED
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>

#include <iostream>
#include <string>
#include <vector>

// GRAPHICS:
#include "Graphics.h"
// GAME LOGIC:
#include "Logic.h"

#define INIT_WIDTH 820
#define INIT_HEIGHT 620

using std::string;
using std::ifstream;
using std::cout;


class Game
{
    SDL_Window *window;
    SDL_Event event;
    SDL_GLContext glcontext;

    shared_ptr<GAME_STATE> gameState;

    Graphics graphics;
    Logic logic;

    void SetBgrnd(float r, float g, float b);
    void Show();
    
public:
    
    Game();
    ~Game();
    bool Init();
    void Loop();
};
