#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <string.h>

#include <GL/glew.h>
#include <GL/gl.h>

#include "ShaderLoader.h"

#define BFG_RS_NONE  0x0   
#define BFG_RS_ALPHA 0x1
#define BFG_RS_RGB   0x2
#define BFG_RS_RGBA  0x4

#define WIDTH_DATA_OFFSET  20
#define MAP_DATA_OFFSET 276

#define BFG_MAXSTRING 255


#ifndef GL_CLAMP_TO_EDGE 
#define GL_CLAMP_TO_EDGE 0x812F 
#endif

using std::ifstream; using std::string;

class FontHandler
{

public:

    FontHandler();
    bool Load(string & fname);
    void SetColor(float red, float green, float blue);
    void Select();                
    void Bind();                  
    void SetBlend();              
    static void Set(float * vertices, size_t vSize, GLuint shaderID, short SHADER_T = 1, GLenum SHAPE = GL_QUADS);
    void Print(const char *text, float x, float y, float len, GLuint texShader); 
    int  GetWidth(char *text);

private:

    int cellX,cellY,yOffset,rowPitch;
    char base;
    char width[256];   
    void CalcUnit(const char *text, int sLen, float length);
    float unit;
    GLuint texID;
    float rowFactor,colFactor;
    int renderStyle;
    float rd,gr,bl;
};
    
