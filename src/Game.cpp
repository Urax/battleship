#include "Game.h"

Game::Game()
{
    gameState = std::make_shared<GAME_STATE>(SETUP);    
}

bool Game::Init()
{
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 1 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
    SDL_Init(SDL_INIT_VIDEO);

    window = SDL_CreateWindow("Battleship",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
            INIT_WIDTH, INIT_HEIGHT,
            SDL_WINDOW_OPENGL);
    if (window == NULL) {
        // In the case that the window could not be made...
        printf("Could not create window: %s\n", SDL_GetError());
        return false;
    }
    glcontext = SDL_GL_CreateContext(window);
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
      /* Problem: glewInit failed, something is seriously wrong. */
      fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
    }

    logic = Logic(10, 10);

    bool res = graphics.Init(INIT_WIDTH, INIT_HEIGHT, 
            logic.GetEnemyBoard(), logic.GetPlayerBoard(), 
            logic.GetEvents(), gameState
        );
    if(!res) 
    {
        std::cout << "Shader initialisation failed !" <<std::endl;
        return false;
    }
    graphics.ShareRowColToMouse(logic.GetRowPtr(), logic.GetColPtr());


    return true;
}



void Game::Loop()
{
    bool quit = false;

    while(!quit)
    {

        while(SDL_PollEvent(&event) != 0)
        {
            switch (event.type)
            {
                case SDL_QUIT:
                    quit = true;
                    break;
                default:
                    graphics.Draw();
                    graphics.GetPos();
                    *gameState = logic.CheckEvent();
                    break;
            }
            SDL_GL_SwapWindow(window);
        }
    }
}

Game::~Game()
{
    printf("deleting glcontext...\n");
    SDL_GL_DeleteContext(glcontext);  
    SDL_DestroyWindow(window);
    SDL_Quit();
}
