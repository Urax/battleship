#include "ShaderLoader.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>


Shader::Shader()
{
   float cols[] = {
       0.f, 0.f, 0.f, // black
       1.f, 0.f, 0.f, // red
       0.f, 1.f, 0.f, // green
       0.f, 0.f, 1.f, // blue
       0.f, 0.f, .4f, // dark blue
       0.f, 1.f, 1.f, // aqua
       1.f, 1.f, 0.f, // yellow
       1.f, 0.f, 1.f, // purple
       .3f, .3f, .3f, // grey
       1.f, 1.f, 1.f  // white
   };

   int n = sizeof(cols) / sizeof(*cols);

   colors = new struct Color[n/3];

   for(int i = 0; i < n;)
   {
       int j = i/3;
       colors[j].r = cols[i++];
       colors[j].g = cols[i++];
       colors[j].b = cols[i++];
   }
}

Shader::~Shader()
{
    delete[] colors;
}


bool Shader::ShaderInit()
{
    objShader = LoadShaders("shaders/vertex-shader.glsl", "shaders/obj-fragment-shader.glsl");
    texShader = LoadShaders("shaders/vertex-shader.glsl", "shaders/tex-fragment-shader.glsl");
    if(!objShader & !texShader)
        return 0; // error
    return 1;
}

void Shader::SetBgrnd(COL col)
{
    struct Color col_s = colors[col]; 

    glClearColor(col_s.r, col_s.g, col_s.b, 1.);
    glClear(GL_COLOR_BUFFER_BIT);
}

bool Shader::PrepareImg(string textureName)
{
    GLuint vertexArrayID;
    glGenVertexArrays(1, &vertexArrayID);
    glBindVertexArray(vertexArrayID);

    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// s = x 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);   // and t = y in textures coordinates
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // make the image look less pixelised
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // load and generate the texture
    int width, height, nrChannels;
    unsigned char *data = stbi_load(textureName.c_str(), &width, &height, &nrChannels, 0);

    if (data)
    {
        // GL_RGBA8??
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
        return false;
    }
    stbi_image_free(data);

    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    return true;
}


void Shader::Projection(int width, int height)
{
    glm::mat4 projection = glm::ortho(0.0f, static_cast<float>(width), static_cast<float>(height), 0.0f, -1.0f, 1.0f); 
    glUseProgram(texShader);
    glUniformMatrix4fv(glGetUniformLocation(texShader, "projection"), 1, false, glm::value_ptr(projection));
    glUseProgram(objShader);
    glUniformMatrix4fv(glGetUniformLocation(objShader, "projection"), 1, false, glm::value_ptr(projection));
}


    /*
     *  EXAMPLE:
    float vertices[] = {
        // positions      // colors       // texture coords
         1.f,  1.f,      1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
         1.f, -1.f,      0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
        -1.f, -1.f,      0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
        -1.f,  1.f,      1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left 
    };
    */

void Shader::DrawStrLine(float x, float y, float len, COL col, ORIENT orient)
{
    struct Color col_s = colors[col]; 

    glLineWidth(5.);

    float vertices[] = {
        // positions            // colors        
         x,  y,          col_s.r, col_s.g, col_s.b,      
         x,  y + len,    col_s.r, col_s.g, col_s.b,     
    };
    if(!orient)
    {
        vertices[5] = x + len; vertices[6] = y;
    }

    SetObjShader(vertices, sizeof(vertices), GL_LINES);
}
void Shader::DrawRect(float x, float y, float lenX, float lenY, COL col)
{
    struct Color col_s = colors[col]; 

    float vertices[] = {
        // positions            // colors        
         x + lenX,  y,          col_s.r, col_s.g, col_s.b,      // top right
         x + lenX, y + lenY,    col_s.r, col_s.g, col_s.b,      // bottom right
         x, y + lenY,           col_s.r, col_s.g, col_s.b,      // bottom left
         x,  y,                 col_s.r, col_s.g, col_s.b,      // top left
    };
    SetObjShader(vertices, sizeof(vertices));
}

void Shader::DrawSquare(float x, float y, float len, COL col)
{
    DrawRect(x, y, len, len, col);
}

void Shader::DrawIsTriangle(float x, float y, float lenX, float lenY, COL col, ORIENT orient)
{
    struct Color col_s = colors[col]; 

    float vertices[] = {
        // positions                        // colors        
         x + lenX/2,  y + lenY,          col_s.r, col_s.g, col_s.b,      // top 
         x + lenX, y,                    col_s.r, col_s.g, col_s.b,      // bottom right
         x, y,                           col_s.r, col_s.g, col_s.b,      // bottom left
    };
    if(!orient)
    {
        vertices[0] = x + lenX; vertices[1] = y + lenY/2; // top 
        vertices[5] = x; vertices[6] = y + lenY;     // bottom right
        vertices[10] = x; vertices[11] = y;       // bottom left
    }

    SetObjShader(vertices, sizeof(vertices), GL_TRIANGLES);
}

void Shader::DrawEqTriangle(float x, float y, float len, COL col, ORIENT orient)
{
    if(!orient)
        DrawIsTriangle(x, y, len * MAGIC_NUM, len, col, orient);  
    else
        DrawIsTriangle(x, y, len, len * MAGIC_NUM, col, orient);   
}

void Shader::SetObjShader(float * vertices, size_t vSize, GLenum SHAPE)
{
    
    glUseProgram(objShader);
    FontHandler::Set(vertices, vSize, objShader, OBJ_SHADER, SHAPE);
}


void Shader::SetTexShader(float * vertices, size_t vSize)
{
    
    glUseProgram(texShader);
    FontHandler::Set(vertices, vSize, texShader, TEX_SHADER);
}

void Shader::LoadText(string & text, float x, float y, float length, COL col)
{
    struct Color col_s = colors[col];
    glUseProgram(texShader);
    FontHandler fontH;
    string fname = "font/Arial.bff";
    fontH.SetColor(col_s.r, col_s.g, col_s.b);
    if(!fontH.Load(fname)) std::cout << "BAD";
    fontH.Select();
    fontH.Print(text.c_str(), x, y, length, texShader);
}

GLuint Shader::LoadShaders(const char* vertex_file, const char* fragment_file) {

    GLuint vertex_sh_ID = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragment_sh_ID = glCreateShader(GL_FRAGMENT_SHADER);

    string vertex_code;
    ifstream vertex_sh_stream (vertex_file);
    if(vertex_sh_stream.is_open()) {
        std::stringstream sstr;
        sstr << vertex_sh_stream.rdbuf();
        vertex_code = sstr.str();
        vertex_sh_stream.close();
    }
    else {
        cout << "problem with openning the vertex shader!" << std::endl;
        return 0;
    }

    string fragment_code;
    ifstream fragment_sh_stream (fragment_file); 
    if(fragment_sh_stream.is_open()) {
        std::stringstream sstr;
        sstr << fragment_sh_stream.rdbuf();
        fragment_code = sstr.str();
        fragment_sh_stream.close();
    }

    GLint res = GL_FALSE;
    int infolength;
    
    cout << "Compiling vertex shader: " << vertex_file << std::endl;
    char const * vertex_src_ptr = vertex_code.data();
    glShaderSource(vertex_sh_ID, 1, &vertex_src_ptr, NULL);
    glCompileShader(vertex_sh_ID);

    glGetShaderiv(vertex_sh_ID, GL_COMPILE_STATUS, &res);
    glGetShaderiv(vertex_sh_ID, GL_INFO_LOG_LENGTH, &infolength);

    if ( infolength > 0 ){
		std::vector<char> vertex_sh_error(infolength+1);
		glGetShaderInfoLog(vertex_sh_ID, infolength, NULL, &vertex_sh_error[0]);
        cout << &vertex_sh_error[0] << std::endl;
        return 0;
	}

    cout << "Compiling fragment shader: " << fragment_file << std::endl;
    char const * fragment_src_ptr = fragment_code.data();
    glShaderSource(fragment_sh_ID, 1, &fragment_src_ptr, NULL);
    glCompileShader(fragment_sh_ID);

    glGetShaderiv(fragment_sh_ID, GL_COMPILE_STATUS, &res);
    glGetShaderiv(fragment_sh_ID, GL_INFO_LOG_LENGTH, &infolength);

    if ( infolength > 0 ){
		std::vector<char> fragment_sh_error(infolength+1);
		glGetShaderInfoLog(fragment_sh_ID, infolength, NULL, &fragment_sh_error[0]);
        cout << &fragment_sh_error[0] << std::endl;
        return 0;
	}

    cout << "Linking program" << std::endl;
    GLuint program_ID = glCreateProgram();
    glAttachShader(program_ID, vertex_sh_ID);
    glAttachShader(program_ID, fragment_sh_ID);
    glLinkProgram(program_ID);

    glGetProgramiv(program_ID, GL_LINK_STATUS, &res);
    glGetProgramiv(program_ID, GL_INFO_LOG_LENGTH, &infolength);

    if ( infolength > 0 ){
		std::vector<char> program_error(infolength+1);
		glGetShaderInfoLog(program_ID, infolength, NULL, &program_error[0]);
        cout << "PROGRAM" << std::endl;
        cout << &program_error[0] << std::endl;
        return 0;
	}

    glDetachShader(program_ID, vertex_sh_ID);
    glDetachShader(program_ID, fragment_sh_ID);

    glDeleteShader(vertex_sh_ID);
    glDeleteShader(fragment_sh_ID);

    return program_ID;
}

