#pragma once

#include <SDL2/SDL.h>
#include <iostream>
#include <vector>
#include <memory>

#include "Mouse.h"
#include "Bot.h"

using std::vector;


class Logic
{
    shared_board playerBoard;
    shared_board enemyBoard;
    shared_ptr<vector<int * >> amShips;  
    shared_ptr<EVENTS> events;  
    vector<vector<SQ_STATE>> helpBoard; 
    shared_ptr<int> curRow, curCol;
    int rows, columns;
    Bot bot;

    bool CheckBoard(bool player);
    void CheckWin();
    void ResetBoards();
    void InitBoards();

    
public:

    Logic();
    Logic(int rows, int columns);  
    shared_board GetPlayerBoard();
    shared_board GetEnemyBoard();
    shared_ptr<EVENTS> GetEvents();
    shared_ptr<int> GetRowPtr();
    shared_ptr<int> GetColPtr();
    void GenerateBotBoard();
    GAME_STATE CheckEvent();

};
