#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_mouse.h>
#include <iostream>
#include <memory>
#include <vector>

using std::vector; using std::string;
using std::shared_ptr;
using std::cout; using std::endl;

enum SQ_STATE // square state
{
    EMPTY = 0,
    ALIVE = 1,
    HIT = 2,
    MISS = 3
};

enum GAME_STATE
{
    PLAY = 0,
    SETUP = 1,
    OPTIONS = 2,
    MENU = 3,
};

enum EVENTS
{
    NOTHING = 0,
    RESET,
    CHECK,
    SET,
    PLAYER_WIN,
    ENEMY_WIN
};

enum BUTTON_NAME
{
    NONE = 0,
    ACCEPT = 1
};

using shared_board = shared_ptr<vector<vector<SQ_STATE>>>;

struct BoardsInfo
{
    float x;
    float plY; // player Y
    float enY; // enemy Y
    float width;
    float height;
    float unit;
};

struct Button
{
    BUTTON_NAME name;
    string text;
    float x;
    float y;
    float lenX;
    float lenY;
};

class Mouse
{

    int x, y;

    BUTTON_NAME btnName;
    shared_board playerBoard;
    shared_board enemyBoard;
    shared_ptr<EVENTS> events;
    shared_ptr<GAME_STATE> gameState;
    shared_ptr<BoardsInfo> bsInfo;
    shared_ptr<vector<Button>> buttons;
    shared_ptr<int> row, column;

    bool CheckInRange(float rangeX0, float rangeX1, float rangeY0, float rangeY1);
    bool CheckInBoard();
    void ChangeBoard(bool leftB);
    BUTTON_NAME CheckButtons();
public:

    Mouse();
    Mouse(
            shared_board enemyBoard, 
            shared_board playerBoard, 
            shared_ptr<BoardsInfo> bsInfo, 
            shared_ptr<vector<Button>> buttons,
            shared_ptr<EVENTS> events,
            shared_ptr<GAME_STATE> gameState
        );
    void ShareRowCol(shared_ptr<int> row, shared_ptr<int> col);
    BUTTON_NAME GetPos();
};
