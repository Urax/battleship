<div align="center">

# Battleship

Battleship game written in C++ OpenGL

### Compilation

</div>

Requirements:
- cmake > 3.16 
- SDL 2
- glm

### Build Setup:

Building with MinGW:
```BatchFile
 
cmake -S. -B./build -G "MinGW Makefiles" -DCMAKE_EXPORT_COMPILE_COMMANDS=YES

cd ./build

mingw32-make

battleship.exe
```



